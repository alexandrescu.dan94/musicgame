package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class MusicExerciseApplication {

    public static void main(String[] args) {
        SpringApplication.run(MusicExerciseApplication.class, args);
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src/main/resources/input.txt"))) {

            List<String> lyrics = new LinkedList<>();
            while (bufferedReader.ready()) {
                lyrics.add(bufferedReader.readLine());
            }

            // OPTIMIZAT

            Map<Integer, String> intermediateResult = new HashMap<>();
            Map<String, Integer> finalResult = new HashMap<>();

            for(int i = 0; i < lyrics.size()-1; i++) {
                if (!intermediateResult.isEmpty()) {
                    if (intermediateResult.containsValue(lyrics.get(i))) {
                        finalResult.put(lyrics.get(i), finalResult.getOrDefault(lyrics.get(i), 1) + 1);
                    }
                }
                intermediateResult.put(i, lyrics.get(i));
            }

            String result = finalResult.entrySet().stream().max((entry1, entry2) -> entry1.getKey().length() > entry2.getKey().length() ? 1 : -1).get().getKey();

            System.out.println(finalResult);

            System.out.println("Result: " + result);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
